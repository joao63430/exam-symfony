<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\ForumRepository;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class HomeController extends AbstractController
{

    private ForumRepository $forumRepository;
    private MessageRepository $messageRepository;

    /**
     * @param ForumRepository $forumRepository
     * @param MessageRepository $messageRepository
     */
    public function __construct(ForumRepository $forumRepository, MessageRepository $messageRepository)
    {
        $this->forumRepository = $forumRepository;
        $this->messageRepository = $messageRepository;
    }


    #[Route('/', name: 'home')]
    public function index(): Response
    {
        $forums = $this->forumRepository->findAll();

        return $this->render('home/index.html.twig', [
            "forums" => $forums
        ]);
    }

    #[Route('forum/{id}', name: 'forum')]
    public function forum($id, EntityManagerInterface $entityManager, Request $request): Response
    {
        $forum = $this->forumRepository->findOneBy(["id" => $id]);
        $messages = $this->messageRepository->findBy(["forum" => $forum]);


        $message = new Message();
        $message
            ->setCreatedAt(new \DateTime('now'))
            ->setForum($forum)
            ->setUser($this->getUser())
        ;

        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {


            $message = $form->getData();


            $entityManager->persist($message);
            $entityManager->flush();
            $this->addFlash('success', sprintf("Le message a bien été envoyé"));
            return $this->redirectToRoute('forum', ["id" => $id], Response::HTTP_SEE_OTHER);
        }


        return $this->renderForm('home/forum.html.twig', [
            "form" => $form,
            "forum" => $forum,
            "messages" => $messages
        ]);

    }
}
