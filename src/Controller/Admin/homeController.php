<?php

namespace App\Controller\Admin;

use App\Entity\Forum;
use App\Form\Admin\forumType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class homeController extends AbstractController
{
    #[Route('/admin/home', name: 'admin_home')]
    public function index(EntityManagerInterface $entityManager, Request $request): Response
    {
        $forum = new Forum();
        $forum
            ->setCreatedAt(new \DateTime('now'))
            ->setUser($this->getUser())
        ;

        $form = $this->createForm(forumType::class, $forum);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {


            $forum = $form->getData();


            $entityManager->persist($forum);
            $entityManager->flush();
            $this->addFlash('success', sprintf("Le message a bien été envoyé"));
            return $this->redirectToRoute('forum', ["id" => $form->getData()->getId()], Response::HTTP_SEE_OTHER);
        }


        return $this->renderForm('admin/home/index.html.twig', [
            "form" => $form,
            "forum" => $forum,
        ]);
    }
}
