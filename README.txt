créer .env.local

```
DATABASE_URL="mysql://root:@127.0.0.1:3306/symfo_exam?serverVersion=5.7&charset=utf8mb4"
```

________________________________________________________________________________________________________________________
Commande pour la création de la base de donnée:

```
 php bin/console doctrine:database:create
 php bin/console make:migration
 php bin/console doctrine:migrations:migrate

```

________________________________________________________________________________________________________________________
installer composer et yarn avec :

```
 composer install
 yarn install
```

________________________________________________________________________________________________________________________
installer les fixtures avec :

```
 symfony console hautelook:fixtures:load --purge-with-truncate
```

________________________________________________________________________________________________________________________
Lancer le serveur avec :

```
symfony serve
```
